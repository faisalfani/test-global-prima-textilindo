import { h } from 'preact';
import style from './style.css';
import {Table} from 'reactstrap'
import data from '../../data.json'

const Home = () => {

	const {location , proformaInfoId, proformaItem} = data
	proformaItem.forEach(data=>{
		data.product_stock = JSON.parse(data.product_stock)
		data.items = JSON.parse(data.items)
	})
	return(
	<div class={style.home}>
	
		<h1>Table</h1>
		<Table striped bordered>
			<thead >
				<tr>
						{location.map(data=>{
							return <th key={data.id}>{data.name}</th>
						})}
						<th>Categories</th>
						<th>Product</th>
						<th>Total Stock</th>
						<th>Percent %</th>
						<th>Total Order</th>
						{proformaInfoId.map((data,key)=>{
							return <th key={key}>{data}</th>
						})}
				</tr>
			</thead>
			<tbody>
				{proformaItem.map((data)=>{
					console.log(data.product_stock)
					const total = data.product_stock[0][1] ? data.product_stock[0][1]+data.product_stock[1][2] : data.product_stock[0][2] + data.product_stock[1][1]
					return(
						<tr>
							{data.product_stock.map(res=>{
								return(<>
									<td>{res[1]?res[1] : res[2]}</td>
									</>
								)
							})}
							<td>{data.categoryDescription}</td>
							<td>{data.productDescription}</td>
							<td>{total}</td>
							<td>{((data.items.length/total)*100).toFixed(2)}%</td>
							<td>{data.items.length}</td>
							<td>{data.items[0][387578]? data.items[0][387578] : 0}</td>
							<td>{data.items[0][387577]? data.items[0][387577] : 0}</td>
							<td>{data.items[0][387576]? data.items[0][387576] : 0}</td>
						
						</tr>
					)
				})}
			</tbody>
		</Table>
	</div>
)};

export default Home;
